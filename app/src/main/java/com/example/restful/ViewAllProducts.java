package com.example.restful;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class ViewAllProducts extends AppCompatActivity {

    private static final String TAG = "JSONPHPMySQL"; // Log TAG
    // Progress Dialog
    private ProgressDialog pDialog;
    private ListView listViewProducts;
    ArrayList<HashMap<String, String>> productsList = new ArrayList<HashMap<String,
            String>>();
    boolean status = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_products);
        listViewProducts = (ListView) findViewById(R.id.listView1);
        // url to get all products list
        final String url = "http://10.0.2.2/android/get_all_products.php";
        status = checkNetworkConenction();
        if (status) {
            Toast.makeText(getApplicationContext(), "Network Available",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Network Not Available",
                    Toast.LENGTH_LONG).show();
        }
        new LoadAllProducts().execute(url);
        listViewProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long
                    id) {
                // When clicked, show a toast with the TextView text
                TextView textViewName = (TextView) view.findViewById(R.id.txtName);
                TextView textViewPrice = (TextView) view.findViewById(R.id.txtPrice);
                String message = "You clicked # " + position + " Products Name : " +
                        textViewName.getText() + ", Price : " + textViewPrice.getText().toString();
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public final boolean checkNetworkConenction() {
        ConnectivityManager ConnectionManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            //Network Available
            return true;
        } else {

            return false;
        }
    }
    /**
     * Background Async Task to Load all product by making HTTP Request
     */
    class LoadAllProducts extends AsyncTask<String, Void, Integer> {
        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ViewAllProducts.this);
            pDialog.setTitle("Please Wait..");
            pDialog.setMessage("Loading products. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected Integer doInBackground(String... params) {
            InputStream inputStream = null;
            HttpURLConnection urlConnection = null;
            Integer result = 0;
            try {
                URL url = new URL(params[0]);
                try {
                    urlConnection = (HttpURLConnection) url.openConnection();
                    /* optional request header */
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    /* optional request header */
                    urlConnection.setRequestProperty("Accept", "application/json");
                    /* for Get request */
                    urlConnection.setRequestMethod("GET");
                    int statusCode = urlConnection.getResponseCode();
                    /* 200 represents HTTP OK */
                    if (statusCode == 200) {
                        inputStream = new BufferedInputStream(urlConnection.getInputStream());
                        String response = convertInputStreamToString(inputStream);
                        parseResult(response);
                        result = 1; // Successful
                    } else {
                        result = 0; //"Failed to fetch data!";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(ViewAllProducts.this, e.toString(), Toast.LENGTH_LONG).show();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Toast.makeText(ViewAllProducts.this, e.toString(), Toast.LENGTH_LONG).show();
            }
            return result;
        }
        @Override
        protected void onPostExecute(Integer result) {
            // Close progress dialog
            pDialog.dismiss();
            /* Download complete. Lets update UI */
            if (result == 1) {
                //ListView
                // keys of hashmap
                String[] from = {"name", "price"};
                // view id's to which data to be binded
                int[] to = {R.id.txtName, R.id.txtPrice};
                //Creating Adapter
                ListAdapter adapter = new SimpleAdapter(ViewAllProducts.this, productsList,
                        R.layout.list_products, from, to);
                //Setting Adapter to ListView
                listViewProducts.setAdapter(adapter);
            } else {
                Log.e(TAG, "Failed to fetch data!");
            }
        }
        private String convertInputStreamToString(InputStream inputStream) throws
                IOException {
            BufferedReader bufferedReader = new BufferedReader(new
                    InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }
            /* Close Stream */
            if (null != inputStream) {
                inputStream.close();
            }
            return result;
        }
        private void parseResult(String result) {
            try {
                //for JSON Object data
                JSONObject response = new JSONObject(result);
                JSONArray json_lists = response.optJSONArray("products");
                //for JSON Array data
                //JSONArray json_lists = new JSONArray(result);
                for (int i = 0; i < json_lists.length(); i++) {
                    JSONObject json_obj = json_lists.optJSONObject(i);
                    //for JSON Array data
                    String prod_name = json_obj.optString("name");
                    String prod_price = json_obj.optString("price");
                    //add to data for Adapter of listview
                    HashMap<String, String> product = new HashMap<String, String>();
                    product.put("name", prod_name);
                    product.put("price", prod_price);
                    productsList.add(product);
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
        }
    }
}